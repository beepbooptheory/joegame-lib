import addAllNPCsFromLayer from './actions/addAllNPCsFromLayer'
import addAllTweetConvosFromLayer from './actions/addAllTweetConvosFromLayer'
import addAllObjectsFromLayer from './actions/addAllObjectsFromLayer'
import addAllPlatformsFromLayer from './actions/addAllPlatformsFromLayer'
import addPlayerToLevel from './actions/addPlayerToLevel'
import createLevelPhysics from './factories/createLevelPhysics'
import joegameFacade from './joegameFacade'
import createDepthMap from './utils/createDepthMap'
import runCinematicNode from './actions/runCinematicNode'
import createTweetConvo from './factories/createTweetConvo'
import shaders from './shaders/index'
import { parsewikidata as parseOrgWikiData } from "./utils/parseWikiData"
import { parseCSVRowsToWikiData } from "./utils/parseCSVRowsToWikiData"
import MIDIPlayer from 'timidity-wasm'
// import Toner from './sound/Toner'
console.log('yo')

async function playMIDIFile(path: string) {
    const mplayer = await MIDIPlayer.createMIDIPlayer("./gravis.cfg")
    mplayer.load(path)
    return mplayer
}

export {
    joegameFacade,
    runCinematicNode,
    createTweetConvo,
    parseOrgWikiData, //
    parseCSVRowsToWikiData,
    shaders,
    playMIDIFile
}
