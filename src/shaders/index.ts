import 'phaser'
import RainfallPostFX from './RainfallPostFX'
import { WaterDropPostFX } from './WaterDropPostFX'
import { DoomWipePostFX } from './DoomWipePostFX'
import { PageCurlPostFX } from './PageCurlPostFX'
import { WipePostFX } from './WipePostFX'
import PlasmaPostFX from './PlasmaPostFX'
import Blobs from './Blobs'

const shaders = {
    RainfallPostFX,
    WaterDropPostFX,
    DoomWipePostFX,
    PageCurlPostFX,
    WipePostFX,
    PlasmaPostFX,
    Blobs
}

export default shaders
